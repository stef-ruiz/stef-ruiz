# About me! 

My name is Stefano (Stef) Ruiz. I am part of the Business Development team here at GitLab. Some of my hobbies include Mountain Biking, Rock Climbing, and Summiting Peaks in my local state of Utah. 

In the past, I was semi-professional mountain biker, bike mechanic, and hospital coordinator. My passion in life is to spread "Leave No Trace" principles, and share my love of the outdoors. 

## My focus is split as follows: 
* Process: 25%
* Collaboration: 25%
* Continuous Learning: 25%
* Positivity: 25% 

## Ways of Working - P2C2

* **Process** - Optimize basic processes to be as effective as possible to reach intended outcomes. Always default to transparency, we should have [a reason for not making something public](https://about.gitlab.com/handbook/communication/#not-public). We should strive to make the most optimal processes from each team and individuals completely transparent as it may help others do their best. 

* **Positivity** - In the face of change, always look at the brightside. Change is inevitable, staying positive is a conscious choice. 

* **Collaboration** - In the words of Daniel Croft, former director of engineering productivity, "I believe in local optimums for teams, each team is made up of unique individuals with unique strengths and weaknesses." Use the unique strengths of the team to better each other. 

* **Continuous Learning** - There's always something to learn. In life, in tech, in the product we sell or use. There are always surprises to be had with the more knowledge you obtain. 

### **Process** - Focusing on the Results of Change 

In the fast-paced world of BDRs, our processes can quickly become overloaded with tasks. It's crucial to ensure these processes are actually driving the results we need. We should regularly evaluate them for effectiveness (achieving intended outcomes) and relevance (addressing our core goals like accounts, SALs, and marketing events).

Luckily, metrics from Outreach, Salesforce, and 6sense provide valuable insights into the impact of our processes. By analyzing this data, we can identify areas for improvement and make data-driven adjustments.

Think of it like software development: processes have intended outcomes, measurable success metrics, and potential risks associated with change. Just as engineers iterate on code, we should be comfortable experimenting, learning from potential missteps, and refining our processes to consistently achieve or exceed quotas.

### **Positivity** - The Light the Fire Within

Being a BDR is **stressful** - with quotas, daily, weekly, and monthly KPIs, it's easy to fall in the trap of negativity. Becoming negative about our role, our leaders, our company can easily spread like viral infection to other people. Being a beacon of positivity is essential for us to stay focused and keeping each others head held high. If we go back to the 2002 Olympics, the motto is *Light the Fire Within*, I want to be able to spread the fire within me to other people so they also stay positive, especially in times of change. 


### **Collaboration** - Out of many, One

I believe we all possess unique strengths and weaknesses. Effectively harnessing the knowledge, experience, and support of others, both for personal growth and the collective benefit of the team, is the essence of successful collaboration.


### **Continuous Learning** - Local Optimums Each Day

My way of working toward continuous learning is providing clear daily goals and measures of success. Focusing on the local optima for the day, will slowly start to add up to the global optimum if and only if we are persistent. 


### This Page

Life is by definition a rate of change for nature hence this page will be updated quite frequently. 


### Updates
- 2024-05-02
- 2024-03-04
- 2024-03-01
- 2024-02-29
